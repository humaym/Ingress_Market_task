package ms14.hometask6;

import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ms14.hometask6.model.Address;
import ms14.hometask6.model.Branch;
import ms14.hometask6.model.Market;
import ms14.hometask6.repository.AddressRepository;
import ms14.hometask6.repository.BranchRepository;
import ms14.hometask6.repository.MarketRepository;
import ms14.hometask6.service.MarketService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class Hometask6Application implements CommandLineRunner {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final AddressRepository addressRepository;
    private final MarketService marketService;

    public static void main(String[] args) {

        SpringApplication.run(Hometask6Application.class, args);
    }

    @Override
    @Transactional
    @org.springframework.transaction.annotation.Transactional
    public void run(String... args) throws Exception {


        Market market = marketRepository.findById(1).get();
        System.out.println(market.getBranches());
        Branch branch=branchRepository.findById(16).get();
        System.out.println(branch);
//
//
//        List<Branch> branchList = branchRepository.findAllByMarket_Id(market.getId());
//        System.out.println(branchList);
//
//
//        Branch branch1 = Branch.builder().
//                address(Address.builder().
//                        branchAddress("Yasamal")
//                        .build())
//                .market(market)
//                .build();
//
//
//        Branch branch2 = Branch.builder()
//                .address(Address.builder()
//                        .branchAddress("Inshaatcilar")
//                        .build())
//                .market(market)
//                .build();
//        Branch branch3 = Branch.builder()
//                .address(Address.builder()
//                        .branchAddress("Merkez")
//                        .build())
//                .market(market)
//                .build();
//
//
//        branchRepository.save(branch1);
//        branchRepository.save(branch2);
//        branchRepository.save(branch3);
//
//        market.getBranches().add(branch1);
//        market.getBranches().add(branch2);
//        market.getBranches().add(branch3);
//
//        marketRepository.save(market);
//        System.out.println(market);
    }
}
