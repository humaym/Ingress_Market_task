package ms14.hometask6.service;

import lombok.RequiredArgsConstructor;
import ms14.hometask6.model.Address;
import ms14.hometask6.model.Branch;
import ms14.hometask6.model.Market;
import ms14.hometask6.repository.AddressRepository;
import ms14.hometask6.repository.BranchRepository;
import ms14.hometask6.repository.MarketRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final AddressRepository addressRepository;


    }

