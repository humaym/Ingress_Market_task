package ms14.hometask6.repository;

import ms14.hometask6.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch, Long> {

    List<Branch> findAllByMarket_Id(Integer id);


    Optional<Branch> findById(int i);
}
