package ms14.hometask6.repository;

import lombok.Data;
import ms14.hometask6.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface MarketRepository extends JpaRepository<Market,Long> {

    Optional<Market> findById(int id);


}
