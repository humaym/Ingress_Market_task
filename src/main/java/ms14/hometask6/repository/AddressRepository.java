package ms14.hometask6.repository;

import ms14.hometask6.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Integer> {

    Address findByBranch_Id(Integer id);
}
